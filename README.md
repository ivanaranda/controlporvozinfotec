# ControlPorVozInfotec

Contro por voz realizado por Infotec

Para agregar comandos específicos:

a) Editar archivo de vocabulario: /Server/commands_en.txt o  /Server/commands_es.txt

b) Ir a http://www.speech.cs.cmu.edu/tools/lmtool-new.html y proporcionar el archivo de vocabulario anterior en la opción "Seleccionar archivo"

c) Descargar archivos (.dic y .lm) y colocarlos en la carperta: /Server/en_model o en la carpeta: /Server/es_model según sea el caso

d) Realizar el cambio de ruta de archivo en la clase: ServerWSVoiceRecognition del proyecto java


Nota: Realizar cambio de ruta/puerto del Servidor en el archivo: Client/ws.js según se requiera

